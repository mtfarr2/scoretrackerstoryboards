//
//  customNameViewController.m
//  scoreTrackerStoryBoards
//
//  Created by Mark Farrell on 2/26/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "customNameViewController.h"

@interface customNameViewController ()

@end

@implementation customNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitBtnTouched:(id)sender {
    NSMutableDictionary* playerNames = [[NSMutableDictionary alloc]init];
    [playerNames setObject: _player1NameField.text forKey:@"player1Name"];
    [playerNames setObject:_player2NameField.text forKey:@"player2Name"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"playerNamesChanged" object:self userInfo:playerNames];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
