//
//  customNameViewController.h
//  scoreTrackerStoryBoards
//
//  Created by Mark Farrell on 2/26/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"


@interface customNameViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *player1NameField;
@property (strong, nonatomic) IBOutlet UITextField *player2NameField;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)submitBtnTouched:(id)sender;

@end
