//
//  ViewController.h
//  scoreTrackerStoryBoards
//
//  Created by Mark Farrell on 2/23/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "customNameViewController.h"

@interface ViewController : UIViewController



@property (strong, nonatomic) IBOutlet UIButton *customNameButton;
- (IBAction)customNameBtnTouched:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *Player1Name;
@property (strong, nonatomic) IBOutlet UILabel *Player2Name;

@property (strong, nonatomic) IBOutlet UIStepper *player1Stepper;
- (IBAction)stepper1Action:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *player1Lbl;
@property (strong, nonatomic) IBOutlet UIStepper *player2Stepper;
- (IBAction)stepper2Action:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *player2Lbl;
@property (strong, nonatomic) IBOutlet UIButton *resetBtn;
- (IBAction)resetBtnTouched:(id)sender;

@end

