//
//  ViewController.m
//  scoreTrackerStoryBoards
//
//  Created by Mark Farrell on 2/23/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNameChange:)
                                                 name:@"playerNamesChanged"
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)stepper2Action:(UIStepper*)player2Stepper {
    _player2Lbl.text = [NSString stringWithFormat:@"%0.f", player2Stepper.value ];
}
- (IBAction)stepper1Action:(UIStepper*)sender {
    _player1Lbl.text = [NSString stringWithFormat:@"%0.f", _player1Stepper.value ];
}
- (IBAction)customNameBtnTouched:(id)sender {
    /*customNameViewController* cnvc = [customNameViewController new];
     [self.navigationController pushViewController:cnvc animated:YES];*/
}
- (void)handleNameChange:(NSNotification *)note {
    NSDictionary *theData = [note userInfo];
    if (theData != nil) {
        _Player1Name.text = [theData objectForKey:@"player1Name"];
        _Player2Name.text = [theData objectForKey:@"player2Name"];
    }
}
- (IBAction)resetBtnTouched:(id)sender {
    _player1Stepper.value = 0;
    _player2Stepper.value = 0;
    _player1Lbl.text = [NSString stringWithFormat:@"0"];
    _player2Lbl.text = [NSString stringWithFormat:@"0"];
}
@end
