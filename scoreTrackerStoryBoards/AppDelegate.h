//
//  AppDelegate.h
//  scoreTrackerStoryBoards
//
//  Created by Mark Farrell on 2/23/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

